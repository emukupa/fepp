import Templates from '../Templates.js';

import { speedAggregated, idleAggregated, min, max } from '../provider/edward.js';

console.log(min, max);

//console.log(lastyearSpeeders[0]);

// import yourWayForSpeeding from "../../../provider/you/?";
// import yourWayForIdling from "../../../provider/you/?";
// import yourWayForStart from "../../../provider/you/?";
// import yourWayForEnd from "../../../provider/you/?";

import SpeedingBarChartComponent from '../components/SpeedingBarChart.js';
import IdlingBarChartComponent from '../components/IdlingBarChart.js';
import MapComponent from '../components/LeafletMap.js';

var HomeView = {
	template: Templates.getView('home'),
	data() {
		return {
			speedingMonths: [],
			idlingMonths: [],
			startCenter: {
				latitude: 0,
				longitude: 0,
			},
			endCenter: {
				latitude: 0,
				longitude: 0,
			},
		};
	},
	components: {
		map: MapComponent,
		'speeding-bar-chart': SpeedingBarChartComponent,
		'idling-bar-chart': IdlingBarChartComponent,
	},

	on: {
		init() {
			this.set('speedingMonths', speedAggregated);
			this.set('idlingMonths', idleAggregated);
			this.set('startCenter', min.location);
			this.set('endCenter', max.location);
		},
		complete() {
			setTimeout(() => {
				this.findAllComponents('map').forEach((map) => map.redraw());
			}, 0);
		},
	},
};

export default HomeView;
